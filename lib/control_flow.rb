# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lower = ("a".."z").to_a.join
  str.delete(lower)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  return str[str.length / 2] if str.length.odd?
  str[(str.length / 2 - 1)..((str.length / 2))]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.count { |char| VOWELS.include?(char) }
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  return 1 if num == 0
  return 1 if num == 1
  num * factorial(num - 1)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr.each_with_index do |entry, idx|
    result += entry
    result += separator if idx != arr.length - 1
  end

  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str_lower = str.downcase.chars
  str_lower.each_index do |idx|
    str_lower[idx] = str_lower[idx].upcase if idx.odd?
  end.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  split_str = str.split(" ")
  split_str.each { |word| word.reverse! if word.length >= 5 }.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).map do |number|
    if number % 3 == 0 && number % 5 == 0
      "fizzbuzz"
    elsif number % 3 == 0
      "fizz"
    elsif number % 5 == 0
      "buzz"
    else
      number
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.each_index.map { |i| arr[-i - 1] }
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2..num / 2).none? { |no| num % no == 0 }
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select { |factor| num % factor == 0 }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  (2..num).select { |factor| num % factor == 0 && prime?(factor) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  (1..num).count { |no| num % no == 0 && prime?(no) }
end

# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  if arr[0].odd? != arr[1].odd?
    return arr[0] if arr[1].odd? == arr[2].odd?
    return arr[1]
  end

  arr[2..-1].each { |entry| return entry if entry.odd? != arr[0].odd? }
end
